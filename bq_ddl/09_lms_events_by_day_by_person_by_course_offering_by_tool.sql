CREATE TABLE IF NOT EXISTS dataset.9_all_events_by_day_by_person_by_course_offering_by_tool (
  /* Event day */
  event_day DATE,

  /* Actor */
  actor_lms_id STRING,

  /* Course offering data */
  course_offering_lms_id STRING,

  is_tool BOOLEAN,
  tool_name STRING,

  /* Tool launches */
  num_tool_launches INT64
)
PARTITION BY event_day
