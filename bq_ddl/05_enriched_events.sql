CREATE TABLE IF NOT EXISTS dataset.5_enriched_events(
  event_time TIMESTAMP,
  event_day DATE,
  event_hour INT64,
  ed_app STRING,
  event_type STRING,
  event_action STRING,
  actor_id STRING,
  actor_ucdm_id INT64,
  actor_sis_id STRING,
  actor_lms_id STRING,
  actor_login STRING,
  actor_type STRING,
  actor_name STRING,
  course_offering_id STRING,
  course_offering_ucdm_id INT64,
  course_offering_lms_id STRING,
  actor_role STRING,
  object_id STRING,
  object_type STRING,
  object_entity STRING,
  object_name STRING,
  object_due_date TIMESTAMP,
  object_max_score STRING,
  object_duration STRING,
  object_entity_id STRING,
  object_filename STRING,
  object_mime_type STRING,
  object_entity_type STRING,
  object_topic_names STRING,
  object_topic_slugs STRING,
  is_part_of_id STRING,
  is_part_of_type STRING,
  is_part_of_entity STRING,
  is_part_of_name STRING,
  assignee_id STRING,
  assignee_type STRING,
  assignable_id STRING,
  assignable_type STRING,
  assignable_entity STRING,
  generated_id STRING,
  generated_type STRING,
  generated_entity STRING,
  generated_grade STRING,
  generated_max_grade STRING,
  generated_scored_by STRING,
  target_id STRING,
  target_type STRING,
  target_watch_percentage STRING,
  target_current_time STRING,
  web_resource_location_type STRING,
  web_resource_location_subtype STRING,
  web_resource_location_name STRING,
  is_tool_launch BOOLEAN,
  launch_app_url STRING,
  launch_app_domain STRING,
  launch_app_name STRING,
  is_tool BOOLEAN,
  tool_name STRING,
  client_ip STRING,
  longitude FLOAT64,
  latitude FLOAT64,
  location_point GEOGRAPHY,
  city_name STRING,
  subdivision_1_iso_code STRING,
  subdivision_1_name STRING,
  postal_code STRING,
  country_iso_code STRING,
  country_name STRING,
  continent_code STRING,
  continent_name STRING,
  time_zone STRING,
  academic_term_name STRING,
  course_unique_name STRING,
  course_offering_status STRING,
  course_subject STRING,
  course_number STRING,
  course_code STRING,
  course_title STRING,
  course_student_enrollments INT64,
  course_instructor_enrollments INT64
)
PARTITION BY event_day
