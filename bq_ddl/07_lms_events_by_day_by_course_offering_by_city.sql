CREATE TABLE IF NOT EXISTS dataset.7_lms_events_by_day_by_course_offering_by_city (
    /* Event day */
  event_day DATE,

  /* Course offering data */
  academic_term_ucdm_id INT64,
  academic_term_name STRING,
  course_offering_lms_id STRING,
  course_offering_ucdm_id INT64,
  course_offering_sis_id STRING,
  course_subject STRING,
  course_number STRING,
  course_code STRING,
  course_title STRING,
  course_unique_name STRING,
  course_offering_status STRING,
  is_course_offering_published BOOLEAN,
  course_student_enrollments INT64,
  course_instructor_enrollments INT64,

  /* Number of events */
  num_events INT64,

  /* Location */
  city_name STRING,
  location_friendly STRING,
  location_point STRING
)
PARTITION BY event_day
