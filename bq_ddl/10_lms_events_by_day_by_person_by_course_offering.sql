CREATE TABLE IF NOT EXISTS dataset.10_lms_events_by_day_by_person_by_course_offering (
  /* Event day */
  event_day DATE,

  /* Actor */
  actor_lms_id STRING,

  course_offering_lms_id STRING,

  /* Number of events */
  num_events INT64,
  num_tool_launches INT64,
  num_learner_activities_submitted INT64,
  num_quizzes_submitted INT64,
  num_things_created INT64,
  num_things_graded INT64
)
PARTITION BY event_day
