-- Create table: 8_lms_events_by_day_by_course_offering

/*
  We'll assume that the `course_offering` table is regeneraed
  everyday with fresh Context store data. This will effectively
  create a point-in-time record of the context data, which then
  becomes relevant for our day-based grouping.

  Unfortunately, we cannot recreate the historical snaphsot data
  since we cannot roll back the context store. So the dimenions
  of this table that are point-in-time sensitive will only have
  value when point-in-time snapshots are available.
*/

WITH dates AS (
  SELECT
    event_day
  FROM
    UNNEST( GENERATE_DATE_ARRAY( DATE '2020-01-01', CURRENT_DATE(), INTERVAL 1 DAY ) ) AS event_day
  -- TO DO: Write a version of this that works with a single day.
  --
),

course_offerings AS (
  SELECT
    /* Academic term information */
    co.academic_term_ucdm_id
    , co.academic_term_name

    /* Course offering IDs */
    , co.course_offering_lms_id
    , co.course_offering_ucdm_id
    , co.course_offering_sis_id

    /* Course offering metadata. */
    , co.course_subject
    , co.course_number
    , co.course_code

    , co.course_title
    , co.course_unique_name

    /*
      The following attributes' value depends on point in time snapshots
      of the Context store data.
    */
    , co.course_offering_status
    , co.is_course_offering_published

    , co.course_student_enrollments
    , co.course_instructor_enrollments

  FROM
    dataset.01_course_offerings co
    -- TO DO: determine some window of eligible course course_offerings
    --        for which to keep generating daily counts. This will get
    --        too big and unwieldy and useless over time if we just
    --        aggregate for all courses each day, even those years in
    --        the past.
),

events_by_day_by_course_offering_by_role AS (
  SELECT
    /* Date & counts */
    r.event_day

    /* Course offering data */
    , r.course_offering_lms_id course_offering_lms_id

    /*
      Count up the number of events for this day
      and for this course by role.
    */
    , SUM(IF(r.actor_role = 'Learner', 1, 0)) learner_events
    , SUM(IF(r.actor_role = 'Instructor', 1, 0)) instructor_events

  FROM
    dataset.05_enriched_events r
--  To do: Make a version of this that focuses on a praticular day,
--         like so:
--  WHERE
--    event_day = '2020-02-15'

  GROUP BY
    r.event_day,
    r.course_offering_lms_id
),

-- Aggregate unique users based on role
unique_users_by_course_offering_by_role_by_day AS (
  SELECT
    /* Date & counts */
    uubcobrbd.event_day

    /*
      Count up the number of events for this day
      and for this course by role.
    */
    , SUM(IF(uubcobrbd.actor_role = 'Learner', 1, 0)) learner_users
    , SUM(IF(uubcobrbd.actor_role = 'Instructor', 1, 0)) instructor_users

    /* Course offering data */
    , uubcobrbd.course_offering_lms_id course_offering_lms_id

  FROM (
    SELECT
      r.event_day,
      r.course_offering_lms_id,
      r.actor_lms_id,
      r.actor_role

    FROM
      dataset.05_enriched_events r
  --  To do: Make a version of this that focuses on a praticular day,
  --         like so:
  --  WHERE
  --    event_day = '2020-02-15'
    GROUP BY
      r.event_day
      , r.course_offering_lms_id
      , r.actor_lms_id
      , r.actor_role
  ) uubcobrbd

  GROUP BY
    uubcobrbd.event_day
    , uubcobrbd.course_offering_lms_id
)

SELECT
  d.event_day event_day

  /* Academic term information */
  , co.academic_term_ucdm_id
  , co.academic_term_name

  /* Course offering IDs */
  , co.course_offering_lms_id
  , co.course_offering_ucdm_id
  , co.course_offering_sis_id

  /* Course offering metadata. */
  , co.course_subject
  , co.course_number
  , co.course_code

  , co.course_title
  , co.course_unique_name

  /*
    As noted above, these values make sense only if we have
    point-in-time snapshots of the course_offering data that
    is updated every day.
  */
  , co.course_offering_status course_offering_status
  , co.is_course_offering_published is_course_offering_published

  , COALESCE(co.course_student_enrollments, 0) course_student_enrollments
  , COALESCE(co.course_instructor_enrollments, 0) course_instructor_enrollments

  /*
    Counts of course-centric events for that day.
  */
  , COALESCE(ebdbcobr.learner_events + ebdbcobr.instructor_events, 0) num_events

  , COALESCE(ebdbcobr.learner_events, 0) num_learner_events
  , COALESCE(ebdbcobr.instructor_events, 0) num_instructor_events

  , COALESCE(uubcobrbd.learner_users, 0) num_unique_learners
  , COALESCE(uubcobrbd.instructor_users, 0) num_unique_instructors

FROM
  course_offerings co, dates d

LEFT JOIN events_by_day_by_course_offering_by_role ebdbcobr
ON
(co.course_offering_lms_id=ebdbcobr.course_offering_lms_id AND d.event_day=ebdbcobr.event_day)

LEFT JOIN unique_users_by_course_offering_by_role_by_day uubcobrbd
ON
(co.course_offering_lms_id=uubcobrbd.course_offering_lms_id AND d.event_day=uubcobrbd.event_day)
