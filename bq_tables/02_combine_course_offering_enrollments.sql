/*
  Target table: 02_course_offering_enrollments
*/

SELECT
  cscoe.course_offering_ucdm_id AS ourse_offering_ucdm_id
  , udwcoe.course_offering_lms_id AS course_offering_lms_id
  , cscoe.actor_ucdm_id AS actor_ucdm_id
  , udwcoe.actor_lms_id AS actor_lms_id
  , CASE
    WHEN cscoe.actor_full_name IS NOT NULL THEN cscoe.actor_full_name
    ELSE udwcoe.actor_full_name
    END AS actor_full_name
  , CASE
    WHEN cscoe.role IS NOT NULL THEN cscoe.role
    ELSE udwcoe.role
    END AS role
  , CASE
    WHEN cscoe.role_status IS NOT NULL THEN cscoe.role_status
    ELSE udwcoe.role_status
    END AS role_status
FROM
  dataset.udw_course_offering_enrollments udwcoe

LEFT JOIN
  dataset.cs_course_offering_enrollments cscoe
  ON
  (
    udwcoe.course_offering_lms_id=cscoe.course_offering_lms_id
    AND udwcoe.actor_lms_id=cscoe.actor_lms_id
  )
