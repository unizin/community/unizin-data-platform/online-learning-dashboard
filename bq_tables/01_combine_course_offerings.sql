/*
  Target table: 01_course_offerings
*/

SELECT
  csco.academic_term_ucdm_id AS academic_term_ucdm_id
  , CASE
    WHEN csco.academic_term_name IS NOT NULL THEN csco.academic_term_name
    ELSE udwco.academic_term_name
    END AS academic_term_name
  , csco.course_offering_ucdm_id course_offering_ucdm_id
  , csco.course_offering_sis_id
  , udwco.course_offering_lms_id
  , CASE
    WHEN csco.course_unique_name IS NOT NULL THEN csco.course_unique_name
    ELSE udwco.course_unique_name
    END course_unique_name
  , CASE
    WHEN csco.course_offering_status IS NOT NULL THEN csco.course_offering_status
    ELSE udwco.course_offering_status
    END course_offering_status
  , CASE
    WHEN csco.is_course_offering_published IS NOT NULL THEN csco.is_course_offering_published
    ELSE udwco.is_course_offering_published
    END is_course_offering_published
  , udwco.course_subject
  , udwco.course_number
  , CASE
    WHEN csco.course_code IS NOT NULL THEN csco.course_code
    ELSE udwco.course_code
    END course_code
  , CASE
    WHEN csco.course_title IS NOT NULL THEN csco.course_title
    ELSE udwco.course_title
    END course_title
  , udwco.course_student_enrollments
  , udwco.course_instructor_enrollments

FROM
  dataset.udw_course_offerings udwco

LEFT JOIN
  dataset.cs_course_offerings csco ON udwco.course_offering_lms_id=csco.course_offering_lms_id
