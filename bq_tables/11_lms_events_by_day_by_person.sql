-- Create table: 11_lms_events_by_day_by_person

/*
  Reasons why there may be fewer events summed in this
  table than in the 5_enriched_events table:

  * Not every event in 5_enriched_events is an LMS event (and therefore
    has a course_offering_lms_id).

  * Not every LMS event takes place in a course and therefore has a
    course_offering_lms_id.

  * Not every LMS event is performed by an individual (and therefore
    has an actor_lms_id).
*/

WITH dates AS (
  SELECT
    event_day
  FROM
    UNNEST( GENERATE_DATE_ARRAY( DATE '2020-01-01', CURRENT_DATE(), INTERVAL 1 DAY ) ) AS event_day
  -- TO DO: Write a version of this that works with a single day.
  --
),

course_offering_enrollments AS (
  SELECT
  actor_lms_id

  FROM
    (
      /*
        Get the set of actors for the current academic term.
      */
      SELECT
        coe.actor_lms_id
      FROM
        dataset.02_course_offering_enrollments coe
      INNER JOIN
        dataset.01_course_offerings co USING (course_offering_lms_id)
      WHERE
        (co.academic_term_name = 'Spring 2020' OR co.academic_term_name = 'SPRING 2020')
        AND coe.actor_lms_id IS NOT NULL
      GROUP BY
        coe.actor_lms_id

      UNION ALL

      /*
        Get the course offering enrollments active in the last
        30 days only.
      */
      SELECT
        r.actor_lms_id
      FROM
        dataset.05_enriched_events r
      WHERE
        r.event_day >= DATE_SUB(CURRENT_DATE(), INTERVAL 30 DAY)
        AND actor_lms_id IS NOT NULL
      GROUP BY
        actor_lms_id
    )

  GROUP BY
    actor_lms_id
),

events_by_day_by_person AS (
  SELECT

    /* Date */
    r.event_day event_day

    /* Actor id */
    , r.actor_lms_id actor_lms_id

    /* Total number of events for the person */
    , COUNT(r.event_time) num_events

    /* Summarizing things that were done. */
    , SUM(IF(r.web_resource_location_type = 'context_external_tool', 1, 0)) num_tool_launches
    , SUM(IF(r.event_action  = 'Submitted', 1, 0)) num_learner_activities_submitted
    , SUM(IF(r.event_action  = 'Submitted' AND r.assignable_type = 'Assessment', 1, 0)) num_quizzes_submitted
    , SUM(IF(r.event_action  = 'Created', 1, 0)) num_things_created
    , SUM(IF(r.event_action  = 'Graded', 1, 0)) num_things_graded

  FROM
    dataset.05_enriched_events r

  WHERE
    actor_lms_id IS NOT NULL
    AND event_day >= '2020-01-01'

  GROUP BY
    r.event_day
    , r.actor_lms_id
)

-- And include any other course enrollments from the last 60 days
-- where the event count is > 60?

SELECT
  /* Date */
  d.event_day AS event_day

  /* Actor id */
  , coe.actor_lms_id actor_lms_id

  /* Total number of events for the person */
  , COALESCE(ebdbp.num_events, 0) num_events

  /* Summarizing things that were done. */
  , COALESCE(ebdbp.num_tool_launches, 0) num_tool_launches
  , COALESCE(ebdbp.num_learner_activities_submitted, 0) num_learner_activities_submitted
  , COALESCE(ebdbp.num_quizzes_submitted, 0) num_quizzes_submitted
  , COALESCE(ebdbp.num_things_created, 0) num_things_created
  , COALESCE(ebdbp.num_things_graded, 0) num_things_graded

FROM
  dates d,
  course_offering_enrollments coe

LEFT JOIN events_by_day_by_person ebdbp
ON
  (
    d.event_day = ebdbp.event_day
    AND coe.actor_lms_id = ebdbp.actor_lms_id
  )
