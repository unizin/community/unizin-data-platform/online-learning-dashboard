--  7_lms_events_by_day_by_course_offering_by_city

/*
  We'll assume that the `course_offering` table is regeneraed
  everyday with fresh Context store data. This will effectively
  create a point-in-time record of the context data, which then
  becomes relevant for our day-based grouping.

  Unfortunately, we cannot recreate the historical snaphsot data
  since we cannot roll back the context store. So the dimenions
  of this table that are point-in-time sensitive will only have
  value when point-in-time snapshots are available.
*/

WITH dates AS (
  SELECT
    event_day
  FROM
    UNNEST( GENERATE_DATE_ARRAY( DATE '2020-01-01', CURRENT_DATE(), INTERVAL 1 DAY ) ) AS event_day
  -- TO DO: Write a version of this that works with a single day.
  --
),

course_offerings AS (
  SELECT
    /* Academic term information */
    co.academic_term_ucdm_id
    , co.academic_term_name

    /* Course offering IDs */
    , co.course_offering_lms_id
    , co.course_offering_ucdm_id
    , co.course_offering_sis_id

    /* Course offering metadata. */
    , co.course_subject
    , co.course_number
    , co.course_code

    , co.course_title
    , co.course_unique_name

    /*
      The following attributes' value depends on point in time snapshots
      of the Context store data.
    */
    , co.course_offering_status
    , co.is_course_offering_published

    , co.course_student_enrollments
    , co.course_instructor_enrollments

  FROM
    dataset.01_course_offerings co
    -- TO DO: determine some window of eligible course course_offerings
    --        for which to keep generating daily counts. This will get
    --        too big and unwieldy and useless over time if we just
    --        aggregate for all courses each day, even those years in
    --        the past.
),

events_by_day_by_course_offering_by_city AS (
  SELECT
    /* Date & counts */
    r.event_day
    , count(r.event_day) num_events

    /* Course offering data */
    , r.course_offering_lms_id

    /* City data */
    , r.city_name city_name
    , CONCAT( r.city_name, ', ', r.subdivision_1_iso_code) location_friendly
    , r.latitude
    , r.longitude
  FROM
    dataset.5_enriched_events r
    -- TO DO: Write a version of this that works with a single day.
    --
    -- WHERE
    -- event_day = '2020-03-01'
  GROUP BY
    r.event_day
    , r.course_offering_lms_id
    , r.city_name
    , r.subdivision_1_iso_code
    , r.longitude
    , r.latitude
)

SELECT
  /* Date & count */
  d.event_day
  , COALESCE(ebdbcobc.num_events, 0) num_events

  /* Academic term information */
  , co.academic_term_ucdm_id
  , co.academic_term_name

  /* Course offering IDs */
  , co.course_offering_lms_id
  , co.course_offering_ucdm_id
  , co.course_offering_sis_id

  /* Course offering metadata. */
  , co.course_subject
  , co.course_number
  , co.course_code

  , co.course_title
  , co.course_unique_name

  /*
    The following attributes' value depends on point in time snapshots
    of the Context store data.
  */
  , co.course_offering_status
  , co.is_course_offering_published

  , co.course_student_enrollments
  , co.course_instructor_enrollments

  /* City data */
  , COALESCE(ebdbcobc.city_name, 'Unknown') city_name
  , COALESCE(ebdbcobc.location_friendly, 'Unknown') location_friendly
  , CASE
    WHEN ebdbcobc.latitude IS NOT NULL AND ebdbcobc.longitude IS NOT NULL THEN
      CONCAT( ebdbcobc.latitude, ',', ebdbcobc.longitude)
    ELSE '0, 0'
    END location_point

FROM
  course_offerings co, dates d

LEFT JOIN
  events_by_day_by_course_offering_by_city ebdbcobc
    ON
      d.event_day=ebdbcobc.event_day
      AND co.course_offering_lms_id=ebdbcobc.course_offering_lms_id
