# Online learning dashboard

This project generates a set of tables in BigQuery intended to support a Google
Data Studio dashboard to explore and understand use of the LMS and teaching and
learning tools across an institution. To make use of the code in this repo, you
must be using the Unizin Data Platform.

The current status of this repository is *highly experimental*.

In short:

* [Extract data data](https://gitlab.com/unizin/community/unizin-data-platform/online-learning-dashboard/-/blob/master/scripts/export_udw_data.pl) from the Unizin Data Warehouse and Unizin Data Platform
* Upload data to a Google Cloud Storage bucket
* Define a [set of tables](https://gitlab.com/unizin/community/unizin-data-platform/online-learning-dashboard/-/tree/master/bq_ddl) in a BigQuery dataset
* Run the [SQL populate the BigQuery tables](https://gitlab.com/unizin/community/unizin-data-platform/online-learning-dashboard/-/tree/master/bq_tables) with data

# Use

The `export_udw_data.pl` will expect a `config.yaml` file at the root of the
repository with the following format:

```yaml
cs:
  name: # name of the Context store database
  host: # hostname/ip to Context store
  port: # port
  user: # username to access Context store
  pass: # password to access Context store
udw:
  name: # name of the Unizin Data Warehouse database
  host: # hostname/ip to UDW
  port: # port
  user: # username to access UDW
  pass: # password to access UDW
```

Running `export_udw_data.pl` will generate four CSV files in `/gcs-data` that
must be uploaded to a Cloud Storage bucket. You'll then create the corresponding
BQ tables from the CSV files in GCS.

# To do

Nothing about this current project is automated or deployable to a Google Cloud
project. Much work remains to automate every step of this process, from building
tables to transforming the data from the UDP Context and Event stores.
