WITH teaching_assignments AS (
  SELECT
    cse.course_id AS course_offering_lms_id
    , count(cse.user_id) AS num_instructors

  FROM
    enrollment_dim cse
  INNER JOIN role_dim r ON cse.role_id=r.id

  WHERE
    r.base_role_type = 'TeacherEnrollment'
    AND (
      cse.workflow_state    = 'active'
      OR cse.workflow_state = 'completed'
      OR cse.workflow_state = 'invited'
    )

  GROUP BY
    cse.course_id
),

student_enrollments AS (
  SELECT
    cse.course_id AS course_offering_lms_id
    , count(cse.user_id) AS num_students

  FROM
    enrollment_dim cse
  INNER JOIN role_dim r ON cse.role_id=r.id

  WHERE
    r.base_role_type = 'StudentEnrollment'
    AND (
      cse.workflow_state    = 'active'
      OR cse.workflow_state = 'completed'
      OR cse.workflow_state = 'invited'
    )

  GROUP BY
    cse.course_id
)

SELECT
  NULL AS academic_term_ucdm_id
  , at.name AS academic_term_name
  , NULL AS course_offering_ucdm_id
  , co.sis_source_id AS course_offering_sis_id
  , co.id AS course_offering_lms_id
  , co.name || ' (' || co.id || ')' AS course_unique_name

  , CASE
    WHEN co.workflow_state = 'created' THEN 'Created'
    WHEN co.workflow_state = 'claimed' THEN 'Claimed'
    WHEN co.workflow_state = 'deleted' THEN 'Deleted'
    WHEN co.workflow_state = 'available' THEN 'Available'
    WHEN co.workflow_state = 'completed' THEN 'Completed'
    END AS course_offering_status

  , CASE
    WHEN co.workflow_state = 'available' OR co.workflow_state = 'completed' THEN TRUE
    ELSE FALSE
    END AS is_course_offering_published

  , -1 AS course_subject
  , -1 AS course_number

  , co.code AS course_code
  , co.name AS course_title

  , COALESCE(se.num_students, 0) AS course_student_enrollments
  , COALESCE(ta.num_instructors, 0) AS course_instructor_enrollments

FROM course_dim co
INNER JOIN enrollment_term_dim at ON co.enrollment_term_id=at.id
LEFT JOIN student_enrollments se ON co.id=se.course_offering_lms_id
LEFT JOIN teaching_assignments ta ON co.id=ta.course_offering_lms_id
