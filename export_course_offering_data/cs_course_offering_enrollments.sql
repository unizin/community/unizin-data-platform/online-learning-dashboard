SELECT
  kco.id AS course_offering_ucdm_id
  , CASE
    WHEN kco.lms_int_id IS NULL THEN 'unknown_lms_id'
    ELSE kco.lms_int_id
    END AS course_offering_lms_id
  , kp.id AS actor_ucdm_id
  , ? + CAST(kp.lms_ext_id AS BIGINT) AS actor_lms_id
  , CASE
    WHEN last_name IS NOT NULL AND first_name IS NOT NULL THEN CONCAT( last_name, ', ', first_name )
    WHEN last_name IS NOT NULL AND first_name IS NULL THEN CONCAT( last_name, ', Unknown' )
    WHEN last_name IS NULL AND first_name IS NOT NULL THEN CONCAT( 'Unknown, ', first_name )
    ELSE 'Unknown, Unknown'
    END AS actor_full_name
  , CASE
    WHEN cse.role IS NOT NULL THEN cse.role
    ELSE 'Unknown'
    END AS role
  , CASE
    WHEN cse.role_status IS NOT NULL THEN cse.role_status
    ELSE 'Unknown'
    END AS role_status

FROM
  entity.course_section_enrollment cse

INNER JOIN entity.course_section s ON cse.section_id=s.course_section_id
INNER JOIN entity.course_offering co ON s.course_offering_id=co.course_offering_id
INNER JOIN entity.academic_term at ON co.academic_term_id=at.academic_term_id
INNER JOIN entity.person p ON cse.person_id=p.person_id
INNER JOIN keymap.course_offering kco ON co.course_offering_id=kco.id
INNER JOIN keymap.person kp ON p.person_id=kp.id

/*
  Don't filter out any enrollments, no matter
  the status; any user/course combo may be active
  in the LMS.

WHERE
  cse.role_status != 'Withdrawn'
  AND cse.role_status != 'Dropped'
*/

GROUP BY
  kco.lms_int_id
  , actor_lms_id
  , kco.id
  , kp.id
  , actor_full_name
  , cse.role
  , cse.role_status
;
