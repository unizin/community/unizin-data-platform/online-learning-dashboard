SELECT
  -1 AS course_offering_ucdm_id
  , cse.course_id AS course_offering_lms_id
  , -1 AS actor_ucdm_id
  , CAST(? + u.canvas_id AS BIGINT) actor_lms_id
  , CASE
    WHEN u.name IS NOT NULL AND u.name != '' THEN u.name
    ELSE 'Unknown, Unknown'
    END AS actor_full_name
  , r.base_role_type role
  , CASE
    WHEN cse.workflow_state = 'active' THEN 'Enrolled'
    WHEN cse.workflow_state = 'completed' THEN 'Enrolled'
    WHEN cse.workflow_state = 'deleted' THEN 'Dropped'
    WHEN cse.workflow_state = 'creation_pending' THEN 'Not-enrolled'
    WHEN cse.workflow_state = 'inactive' THEN 'Not-enrolled'
    WHEN cse.workflow_state = 'invited' THEN 'Enrolled'
    WHEN cse.workflow_state = 'rejected' THEN 'Dropped'
    ELSE 'Unknown'
    END AS role_status
FROM
  enrollment_dim cse

INNER JOIN role_dim r ON cse.role_id=r.id
INNER JOIN user_dim u ON cse.user_id=u.id

/*
  Don't filter out any enrollments, no matter
  the status; any user/course combo may be active
  in the LMS.

WHERE
  cse.workflow_state    = 'active'
  OR cse.workflow_state = 'completed'
*/

GROUP BY
  cse.course_id
  , actor_lms_id
  , r.base_role_type
  , cse.workflow_state
  , u.name
  , course_offering_ucdm_id
  , actor_ucdm_id
